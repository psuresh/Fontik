PACKAGE = fontik
PACKAGE_VERSION = 0.6.1

PREFIX=/usr/local

PROGRAM = fontik
PROGRAM_SOURCES = builder.gs conflist.gs fontik.gs main.gs xmlproc.vala
PROGRAM_UI = fontik.ui

PROGRAM_DIR = $(PREFIX)/share/fontik
PROGRAM_BIN_DIR = $(PREFIX)/bin
PROGRAM_LIBEXEC_DIR = $(PREFIX)/libexec/fontik
INSTALL_PROGRAM_DIR = $(DESTDIR)$(PROGRAM_DIR)
INSTALL_PROGRAM_BIN_DIR = $(DESTDIR)$(PROGRAM_BIN_DIR)
INSTALL_PROGRAM_LIBEXEC_DIR = $(DESTDIR)$(PROGRAM_LIBEXEC_DIR)
PROGRAM_FILES = $(PROGRAM_SOURCES) \
             $(PROGRAM_UI) \
             Makefile \
             COPYING README
PROGRAM_TGZ = $(PACKAGE)-$(PACKAGE_VERSION).tar.gz

LIBS = --pkg gtk+-2.0 --pkg gee-1.0 --pkg gmodule-2.0 --pkg libxml-2.0

all: $(PROGRAM)

ccode: $(PROGRAM_SOURCES)
	valac --ccode $(LIBS) $^

$(PROGRAM): $(PROGRAM_SOURCES)
	valac -g $(LIBS) $^ -o $(PROGRAM)

dist: $(PROGRAM_FILES)
	tar -cvz $(PROGRAM_FILES) > $(PROGRAM_TGZ)

install: all
	mkdir -p $(INSTALL_PROGRAM_DIR) $(INSTALL_PROGRAM_BIN_DIR) \
		$(INSTALL_PROGRAM_LIBEXEC_DIR)
	cp $(PROGRAM_UI) $(INSTALL_PROGRAM_DIR)
	cp $(PROGRAM) $(INSTALL_PROGRAM_LIBEXEC_DIR)/$(PROGRAM)
	echo -e "#!/bin/sh\ncd $(PROGRAM_DIR)\nexec $(PROGRAM_LIBEXEC_DIR)/$(PROGRAM)" > $(INSTALL_PROGRAM_BIN_DIR)/$(PROGRAM)
	chmod +x $(INSTALL_PROGRAM_BIN_DIR)/$(PROGRAM)

uninstall:
	rm -rf $(INSTALL_PROGRAM_DIR)
	rm -f $(INSTALL_PROGRAM_BIN_DIR)/$(PROGRAM)
	rm -rf $(INSTALL_PROGRAM_LIBEXEC_DIR)

clean:
	rm -f *.c $(PROGRAM)


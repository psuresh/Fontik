[indent=4]

uses Gtk

def on_EditConfList()
    var SelectedFont = ""
    // Check whether there is Configured Fonts
    if FontConfList.size is 0
        var msg = new MessageDialog (null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, "No Configured Font yet")
        msg.run()
        msg.destroy()
        return
    //Edit Conf List
    var dialog = new Dialog.with_buttons("Edit Configured Fonts", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
    var vbox1 = new VBox(false, 0)
    var vbox2 = new VBox(false, 0)
    var label1 = new Label("Remove Configured Fonts")
    var check_disable = new CheckButton.with_label("Disable")
    var hbox1 = new HBox( false, 0)
    var hbox2 = new HBox( false, 0)
    var font_conf_list = new ComboBoxEntry.text()
    font_conf_list.set_name("cmbFontConfList")
    var entry = font_conf_list.get_child() as Entry
    entry.set_text("Select Font")
    var completion = new EntryCompletion()
    entry.set_completion(completion)
    var liststore = font_conf_list.get_model()    
    completion.set_model(liststore)
    completion.set_text_column(0)
    check_disable.set_tooltip_text("Disable the Conf Item for the Selected Font")
    check_disable.toggled += def (x)
        DisableConfToggled(font_conf_list, check_disable)
    for font in FontConfList
        font_conf_list.append_text(font)
    font_conf_list.changed += def (x)
        // print "List Changed"
        SelectedFont = FontListChanged(font_conf_list)
        // print "font selected is: " + SelectedFont
        if SelectedFont is not ""
            FontNamePath : string = get_home_dir(FK_CONF_DIR) + "/" + SelectedFont  + ".conf"
            FontNameLinkPath : string = get_home_dir(FK_CONF_DIR) + "/" + "50-" + SelectedFont  + ".conf"
            if FileUtils.test(FontNameLinkPath, FileTest.EXISTS)
                check_disable.set_active( false)
            else if FileUtils.test(FontNamePath, FileTest.EXISTS)
                check_disable.set_active(true)
        return
    var button1 = new Button.with_label("Remove")
    button1.set_tooltip_text("Remove Selected Entry")
    button1.clicked += def (x)
        RemoveFontConf(font_conf_list)
    hbox1.add(font_conf_list)
    hbox2.pack_start(button1,  false,  false, 0)
    vbox1.pack_start(hbox1,  false,  false, 0)
    vbox2.pack_start(hbox2,  false,  false, 0)
    dialog.vbox.pack_start(label1,  false,  false, 4)
    dialog.vbox.pack_start(vbox1,  false,  false, 2)
    dialog.vbox.pack_start(vbox2,  false,  false, 2)
    dialog.vbox.pack_start(check_disable,  false,  false, 4)
    dialog.vbox.show_all()
    dialog.set_modal(true)
    dialog.set_default_response(ResponseType.OK)
    dialog.run()
    dialog.destroy()

def on_EditBlackList()
    // Edit Black List
    var dialog = new Dialog.with_buttons("Edit Blacklist", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
    var vbox1 = new VBox(false, 0)
    var vbox2 = new VBox(false, 0)
    var label1 = new Label("Remove Blacklisted Fonts")
    var check_blacklist = new CheckButton.with_label("Disable Blacklist")
    check_blacklist.set_tooltip_text("Disable the Blacklist")
    if BlacklistDisabled
        check_blacklist.set_active(true)	
    check_blacklist.toggled += def (x) 
        DisableBlacklistToggled(check_blacklist)
    //BlackListConfLink = get_home_dir(FK_CONF_DIR) + "70-blacklist.conf")
    var hbox1 = new HBox(false, 0)
    var hbox2 = new HBox(false, 0)
    var font_black_list = new ComboBoxEntry.text()
    font_black_list.set_name("cmbFontBlackList")
    var entry = font_black_list.get_child() as Entry
    entry.set_text("Select Font")
    var liststore = font_black_list.get_model()
    var completion = new EntryCompletion()
    entry.set_completion(completion)
    completion.set_model(liststore)
    completion.set_text_column(0)
    for font in FontBlackList
        font_black_list.append_text(font)
    var button1 = new Button.with_label("Remove")
    button1.set_tooltip_text("Remove Selected Entry")
    button1.clicked += def (x)
        RemoveFromBlackList(font_black_list)
    hbox1.add(font_black_list)
    hbox2.pack_start(button1, false, false,0)
    vbox1.pack_start(hbox1, false, false,0)
    vbox2.pack_start(hbox2, false, false,0)
    dialog.vbox.pack_start(check_blacklist, false,  false, 4)
    dialog.vbox.pack_start(label1, false,  false, 4)
    dialog.vbox.pack_start(vbox1, false,  false, 2)
    dialog.vbox.pack_start(vbox2, false,  false, 2)
    dialog.vbox.show_all()
    dialog.set_modal(true)
    dialog.set_default_response(ResponseType.OK)
    dialog.run()
    dialog.destroy()

def on_AddToBlackList()
    if BlacklistDisabled
        var msg = new MessageDialog (null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE,  "Blacklist is Disabled.\nEnable it via Menu->Edit Blacklist")
        msg.run()
        msg.destroy()
        return
    var font_name = on_FontList_changed(font_list)
    if font_name is ""
        combo_entry.set_text("No Font")
        print "No font"
    if not FontBlackList.contains(font_name) and font_name is not ""
        var dialog = new Dialog.with_buttons("Blacklist Fonts", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
        var label = new Label("")
        label.set_markup("Do you want to Blacklist the font <b>" + font_name + "?</b>")
        dialog.vbox.add(label)
        dialog.show_all()
        dialog.set_size_request(120,120)
        var result = dialog.run()
        dialog.destroy()
        if result is ResponseType.OK
            // The user clicked Ok
            FontBlackList.add(font_name)
            // Delete the font from FontList
            var index = font_list.get_active()
            font_list.remove_text(index)
            font_list.set_active(0)
            print font_name
            FontList.remove(font_name)
            if FontList.size is 0
                HidePane()
                combo_entry.set_text("No Font")
                SetSampleText("Sans")
            SaveFontBlackList()
            SaveBlackListConf()

def FontListChanged(combo : ComboBoxEntry) : string
    //print "ConfList Changed"
    var active_text = combo.get_active_text()
    index : int = combo.get_active()
    var SelectedFont = ""
    if index > -1
        SelectedFont = active_text
    return SelectedFont

def RemoveFontConf(combo : ComboBoxEntry)
    // Remove Conf Handler
    var font_name = combo.get_active_text()
    // Remove from FontConfList
    if font_name in FontConfList
        FontNamePath : string = get_home_dir(FK_CONF_DIR) + "/" + font_name.replace(" ","-") + ".conf"
        FontNameLinkPath : string = get_home_dir(FK_CONF_DIR) + "/" + "50-" + font_name.replace(" ","-")  + ".conf"
        var dialog = new Dialog.with_buttons("Remove Configured Fonts", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
        var label = new Label("")
        label.set_markup("Delete Configuration for the font\n <b>" + font_name + "?</b>"  )
        dialog.vbox.add(label)
        dialog.show_all()
        dialog.set_size_request(120,120)
        dialog.set_default_response(ResponseType.CANCEL)
        var result = dialog.run()
        dialog.destroy()
        if result is ResponseType.OK
            if FileUtils.test(FontNameLinkPath, FileTest.EXISTS)
                print "Removing Conf link file %s" , FontNameLinkPath
                FileUtils.unlink(FontNameLinkPath)
            if FileUtils.test(FontNamePath, FileTest.EXISTS)
                print "Removing Conf file %s" , FontNamePath
                FileUtils.unlink(FontNamePath)
            print "Removing %s..." , font_name
            FontConfList.remove(font_name)
            var index = combo.get_active()
            combo.remove_text(index)
            combo.set_active(0)
            // print "Font is: " + combo.get_active_text()
            SaveFontConfList()
            on_FontList_changed(font_list)
            reset_fc_cache()
            if FontConfList.size is 0
                var entry = combo.get_child() as Entry
                entry.set_text("Select Font")

def RemoveFromBlackList(combo : ComboBoxEntry)
    // Remove Conf Handler
    var font_name = combo.get_active_text()
    // print "Font is: %s" , font_name
    //Remove from FontBlackList
    if font_name in FontBlackList
        var dialog = new Dialog.with_buttons("Remove From Blacklist", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
        var label = new Label("")
        label.set_markup("Remove <b>" + font_name + "</b> from Blacklist")
        dialog.vbox.add(label)
        dialog.show_all()
        dialog.set_size_request(120,120)
        var result = dialog.run()
        dialog.destroy()
        if result is ResponseType.OK
            FontBlackList.remove(font_name)
            var index = combo.get_active()
            combo.remove_text(index)
            combo.set_active(0)
            SaveBlackListConf()
            SaveFontBlackList()
            reset_fc_cache()
            if FontBlackList.size is 0
                var entry = combo.get_child() as Entry
                entry.set_text("No Font")

def DisableConfToggled(combo : ComboBoxEntry, check_button : CheckButton)
    // Disable/Enable Font Conf
    var font_name = combo.get_active_text()
    if font_name in FontConfList
        var SymLink = get_home_dir(FK_CONF_DIR) + "/" + "50-" + font_name.replace(" ","-")  + ".conf"
        print "Font is: " + font_name
        print "Symlink is: " + SymLink
        if check_button.get_active()
            if FileUtils.test(SymLink, FileTest.EXISTS)
                // Delete SymLink to disable
                print "Deleting symlink...."
                FileUtils.unlink(SymLink)
        else 
            if not FileUtils.test(SymLink, FileTest.EXISTS)
                // Make Sym Link to enable
                print "Making symlink..."
                FileUtils.symlink(get_home_dir(FK_CONF_DIR) + "/" + font_name.replace(" ","-") + ".conf", SymLink)

def DisableBlacklistToggled(check_button : CheckButton)
    // Disable/Enable Font Conf
    var disabled = check_button.get_active()
    if disabled
        BlacklistDisabled = true
    else
        BlacklistDisabled = false
    if FontBlackList.size is not 0
        var SymLink = get_home_dir(FK_CONF_DIR) + "/" + "70-blacklist.conf"
        var BlackListFile = get_home_dir(FK_CONF_DIR) + "/" + "blacklist.conf"
        print "\nBlacklist symlink: " + SymLink
        if disabled
            if FileUtils.test(SymLink, FileTest.EXISTS)
                //Delete SymLink to disable
                print "Deleting symlink...."
                FileUtils.unlink(SymLink)
        else
             if not FileUtils.test(SymLink, FileTest.EXISTS)
                 // Make Sym Link to enable
                 print "Making symlink..."
                 FileUtils.symlink(BlackListFile, SymLink)
        reset_fc_cache()


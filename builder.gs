[indent=4]

uses Gtk

builder : Builder

def builder_init( args : array of string)
    Gtk.init (ref args)
    try
        builder = new Builder()
        builder.add_from_file("fontik.ui")
        builder.connect_signals (null)
    except e : Error
        print "Could not load UI: %s\n", e.message
   
def get_widget(name : string) : GLib.Object
    var wgt = builder.get_object(name)
    return wgt

def on_main_window_destroy (widget : Widget)
    Gtk.main_quit ()




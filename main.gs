[indent=4]

uses Gtk
uses Pango

const USER_FONTCONF : string = "~/.fonts.conf"
const USER_FONTCONF_BACKUP : string = "~/.fonts.conf.fontik.bak"
const FK_DIR : string =   "~/.fontik"
const FK_CONF_DIR : string = FK_DIR + "/conf.d"
const FK_FONTS : string =  FK_DIR + "/fonts.xml"
const FK_BLACK_LIST : string =  FK_DIR + "/blacklist.xml"
const FC_INCLUDE_LINE : string = "<include ignore_missing=\"yes\">" + FK_CONF_DIR + "</include>"

FontList : list of string
FontConfList : list of string
FontBlackList : list of string
FontName : string
BlacklistDisabled : bool
SampText : string

main_window : Window
note_book : Notebook
font_samp_text : Label
lbl_range1 : Label
lbl_range2 : Label
font_list : ComboBoxEntry
combo_entry : Entry
frame1 : Frame
frm_autohint : Frame
frm_adv_hint : Frame
frm_adv_smooth : Frame
frm_synth : Frame
rb_auto_hint : RadioButton
rb_autohint_medium : RadioButton
adv_smooth_disable : CheckButton
adv_smooth_enable : CheckButton
font_smoothing : CheckButton
adv_hint_mode : CheckButton
adv_smooth_mode : CheckButton
enable_synth : CheckButton
uni_scale : CheckButton
uni_slant : CheckButton
scale_x : SpinButton
scale_y : SpinButton
slant_x : SpinButton
slant_y : SpinButton
adv_hint_range1 : SpinButton
adv_hint_range2 : SpinButton
adv_smooth_range1 : SpinButton
adv_smooth_range2 : SpinButton
adj_scale_x : Adjustment
adj_scale_y : Adjustment
adj_slant_x : Adjustment
adj_slant_y : Adjustment
adj_hint_range1 : Adjustment
adj_hint_range2 : Adjustment
adj_smooth_range1 : Adjustment
adj_smooth_range2 : Adjustment
reset_scaling : Button
lang_code : Entry
gen_family : ComboBox
adv_hint_range_combo : ComboBox
adv_hint_method_combo : ComboBox
adv_smooth_range_combo : ComboBox
status_bar : Statusbar

init 
    save_config()
    builder_init(args)

    main_window = get_widget("mainWindow") as Window
    note_book = get_widget("nbConfFont") as Notebook
    font_samp_text = get_widget("lblSampText") as Label
    lbl_range1 = get_widget("lblRange1") as Label
    lbl_range2 = get_widget("lblRange2") as Label
    font_list = get_widget("cmbFontList") as ComboBoxEntry
    combo_entry = font_list.get_child() as Entry
    combo_entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, "gtk-apply")
    combo_entry.set_icon_sensitive(Gtk.EntryIconPosition.SECONDARY, false)
    frame1 = get_widget("frame1") as Frame
    frm_autohint = get_widget("frmAutoHint") as Frame
    frm_adv_hint = get_widget("frmAdvHint") as Frame
    frm_adv_smooth = get_widget("frmAdvSmooth") as Frame
    frm_synth = get_widget("frmSynth") as Frame
    scale_x = get_widget("sbScaleX") as SpinButton
    scale_x.set_value(1.00)
    scale_y = get_widget("sbScaleY") as SpinButton
    scale_y.set_value(1.00)
    slant_x = get_widget("sbSlantX") as SpinButton
    slant_x.set_value(0.00)
    slant_y = get_widget("sbSlantY") as SpinButton
    slant_y.set_value(0.00)
    adv_hint_range1 = get_widget("sbAdvHintRange1") as SpinButton
    adv_hint_range1.set_value(6.00)
    adv_hint_range2 = get_widget("sbAdvHintRange2") as SpinButton
    adv_hint_range2.set_value(6.00)
    adv_smooth_range1 = get_widget("sbAdvSmoothRange1") as SpinButton
    adv_smooth_range1.set_value(6.00)
    adv_smooth_range2 = get_widget("sbAdvSmoothRange2") as SpinButton
    adv_smooth_range2.set_value(6.00)
    uni_scale = get_widget("cbScaleLock") as CheckButton
    uni_slant = get_widget("cbSlantLock") as CheckButton
    adj_scale_x = get_widget("adjustment1") as Adjustment
    adj_scale_y = get_widget("adjustment2") as Adjustment
    adj_slant_x = get_widget("adjustment3") as Adjustment
    adj_slant_y = get_widget("adjustment4") as Adjustment
    reset_scaling = get_widget("btnReset2") as Button
    rb_auto_hint = get_widget("rbAutoHint") as RadioButton
    rb_autohint_medium = get_widget("rbAutoMedium") as RadioButton
    adv_smooth_disable = get_widget("chkAdvSmoothDisable") as CheckButton
    adv_smooth_enable = get_widget("chkAdvSmoothEnable") as CheckButton
    font_smoothing = get_widget("chkEnableSmoothing") as CheckButton
    lang_code = get_widget("txtLang") as Entry
    gen_family = get_widget("cmbGenFamily") as ComboBox
    adv_hint_method_combo = get_widget("cmbAdvHintMethod") as ComboBox
    adv_hint_range_combo = get_widget("cmbAdvHintRange") as ComboBox
    adv_smooth_range_combo = get_widget("cmbAdvSmoothRange") as ComboBox
    enable_synth = get_widget("chkSynth") as CheckButton
    adv_hint_mode = get_widget("chkAdvancedHint") as CheckButton
    adv_smooth_mode = get_widget("chkAdvancedSmooth") as CheckButton
    status_bar = get_widget("statusbar") as Statusbar
    SampText = "This is the Sample Text"
    FontList = new list of string
    FontConfList = new list of string
    FontBlackList = new list of string
    combo_entry.set_text("No Font")
    print "\nLoading Font Conf file:"
    LoadConfList(get_home_dir(FK_FONTS), "/fontik/fonts/family", FontConfList)
    print "\nLoading Blacklist file:"
    LoadConfList(get_home_dir(FK_BLACK_LIST), "/fontik/fonts/blacklist/family", FontBlackList)
    if FileUtils.test(get_home_dir(FK_CONF_DIR) + "/" + "70-blacklist.conf", FileTest.EXISTS)
        print "\nBlacklist is Enabled"
        BlacklistDisabled = false
    else
        print "\nBlacklist is Disabled"
        BlacklistDisabled = true
    Gtk.main ()

def add_fc_include_line()
    if not FileUtils.test(get_home_dir(USER_FONTCONF), FileTest.EXISTS)
        print "\nMaking empty user conf file %s", get_home_dir(USER_FONTCONF)
        var f = FileStream.open(get_home_dir(USER_FONTCONF), "w")
        f.puts("<fontconfig>\n</fontconfig>\n")
    tmpname : string = get_home_dir(USER_FONTCONF) + ".fontik.tmp"
    print "\nStarting install, adding %s to %s", FC_INCLUDE_LINE, get_home_dir(USER_FONTCONF)
    print "\nBackup will be saved as %s" , get_home_dir(USER_FONTCONF_BACKUP)
    var tmp = FileStream.open(tmpname, "w")
    s:string
    l:ulong
    try 
        FileUtils.get_contents(get_home_dir(USER_FONTCONF), out s, out l)
    except e : GLib.Error
        print "Error: %s" , e.message
    if s.contains("</fontconfig>")
        s = s.replace("</fontconfig>", "")
        tmp.puts(s)
        tmp.puts(FC_INCLUDE_LINE + "\n")
        tmp.puts("</fontconfig>")
    print "\nSaving backup %s" , get_home_dir(USER_FONTCONF_BACKUP)
    FileUtils.rename(get_home_dir(USER_FONTCONF), get_home_dir(USER_FONTCONF_BACKUP))
    print "\nOverwriting %s", USER_FONTCONF
    FileUtils.rename(tmpname, get_home_dir(USER_FONTCONF))

def check_fc_include_line() : bool
    if not FileUtils.test(get_home_dir(USER_FONTCONF), FileTest.EXISTS)
        print "\nUser conf file %s does not exist" , USER_FONTCONF
        return false
    s:string
    l:ulong
    try
        FileUtils.get_contents(get_home_dir(USER_FONTCONF), out s, out l)
    except e : GLib.Error
        print "Error: %s" , e.message
    if s.contains(FC_INCLUDE_LINE)
        print "\nInclude line exists in %s" , USER_FONTCONF 
        return true
    print "\nInclude line does not exist in %s" , USER_FONTCONF
    return false

def get_home_dir(file:string) : string
    var home_dir = Environment.get_variable ("HOME")
    if (home_dir != null)
        home_dir = Environment.get_home_dir ()
    return file.replace("~", home_dir)

def save_config()
    if not check_fc_include_line()
    add_fc_include_line()
    make_dot_fontik()

def make_dot_fontik()
    if  not FileUtils.test(get_home_dir(FK_DIR), FileTest.EXISTS)
        print "\nCreating %s", FK_DIR
        DirUtils.create(get_home_dir(FK_DIR), 0755)
    if  not FileUtils.test(get_home_dir(FK_CONF_DIR), FileTest.EXISTS)
        print "\nCreating %s", FK_CONF_DIR
        DirUtils.create(get_home_dir(FK_CONF_DIR), 0755)

def reset_fc_cache()
    //Clears all fontconfig cache files in users home directory
    try
        fc_dir : string = Environment.get_variable ("HOME") + "/" + ".fontconfig"
        var _dir = File.new_for_path(fc_dir)
        if FileUtils.test(fc_dir, FileTest.EXISTS)
            var file_list = _dir.enumerate_children (FILE_ATTRIBUTE_STANDARD_NAME, 0, null) 
            file_info : FileInfo
            while ((file_info = file_list.next_file (null)) != null) 
                var file = fc_dir + "/" + file_info.get_name ()
                if file.has_suffix("cache-3")
                    FileUtils.unlink(file)
    except e : GLib.Error
        print "Error: %s\n", e.message

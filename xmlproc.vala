using Gtk;
using GLib;
using Xml;

public static void SaveFontConfList() {
	Xml.Doc* doc = new Xml.Doc("1.0");
	Xml.Node* root = new Xml.Node(null, "fontik");
	doc->set_root_element(root);
	Xml.Node* fonts = root->new_child(null, "fonts", "");
	foreach (string f in FontConfList) {
		fonts->new_child(null, "family", f );
	}
	doc->save_file(get_home_dir(FK_FONTS));
}

public static void SaveFontBlackList() {
	Xml.Doc* doc = new Xml.Doc("1.0");
	Xml.Node* root = new Xml.Node(null, "fontik");
	doc->set_root_element(root);
	Xml.Node* fonts = root->new_child(null, "fonts","");
	Xml.Node* blacklist = fonts->new_child(null, "blacklist", "");
	foreach (string f in FontBlackList) {
		blacklist->new_child(null, "family", f );
	}
	doc->save_file(get_home_dir(FK_BLACK_LIST));
}

public static void SaveBlackListConf() {
	Xml.Doc* doc = new Xml.Doc("1.0");
	Xml.Node* root = new Xml.Node(null, "fontconfig");
        doc->set_root_element(root);
	Xml.Node* select = root->new_child(null, "selectfont", "");
	foreach (string f in FontBlackList) {
		Xml.Node* reject = select->new_child(null, "rejectfont", "");
		Xml.Node* pattern = reject->new_child(null, "pattern", "");
		Xml.Node* patelt =pattern->new_child(null, "patelt", "");
		patelt->set_prop("name", "family");
		patelt->new_child(null, "string", f);
	}
	string BlackListFile = (get_home_dir(FK_CONF_DIR) + "/" + "blacklist.conf");
	string BlackListLink = (get_home_dir(FK_CONF_DIR) + "/" + "70-blacklist.conf");
	stdout.printf("%s\n", BlackListFile);
	stdout.printf("%s\n", BlackListLink);
	doc->save_file(BlackListFile);
	if (!BlacklistDisabled) {
		if (!FileUtils.test(BlackListLink, FileTest.EXISTS))
			FileUtils.symlink(BlackListFile, BlackListLink);
	}
}

public static void SaveConf(string font) {
	// Save cont entry for font
	string font_name = font;
	// Hint Method. 1:No Hinting, 2:Auto Hinting, 3:Native Hinting
	int HintMethod = 1;
	// Autohint Style: Default Medium
	string HintStyle = "";
	var rb_no_hint = get_widget("rbNoHint") as RadioButton;
	var rb_nat_hint = get_widget("rbNatHint") as RadioButton;
	var rb_autohint_none = get_widget("rbAutoNone") as RadioButton;
	var rb_autohint_slight = get_widget("rbAutoSlight") as RadioButton;
	var rb_autohint_medium = get_widget("rbAutoMedium") as RadioButton;
	var rb_autohint_full = get_widget("rbAutoFull") as RadioButton;
	var text_alias = get_widget("txtAlias") as Entry;
	if (rb_no_hint.get_active())
		HintMethod = 0;
	if (rb_auto_hint.get_active())
		HintMethod = 1;
	if (rb_nat_hint.get_active())
		HintMethod = 2;
	int AdvHintMethod = adv_hint_method_combo.get_active();
	if (rb_autohint_none.get_active())
		HintStyle = "hintnone";
	if (rb_autohint_slight.get_active())
		HintStyle = "hintslight";
	if (rb_autohint_medium.get_active())
		HintStyle = "hintmedium";
	if (rb_autohint_full.get_active())
		HintStyle = "hintfull";
	int AdvHintRange1 = (int) adv_hint_range1.get_value();
	int AdvHintRange2 = (int) adv_hint_range2.get_value();
	int AdvSmoothRange1 = (int) adv_smooth_range1.get_value();
	int AdvSmoothRange2 = (int) adv_smooth_range2.get_value();
	double ScaleX = scale_x.get_value();
	double ScaleY = scale_y.get_value();
	double SlantX = slant_x.get_value();
	double SlantY = slant_y.get_value();
	string Alias = text_alias.get_text();
	string LangCode = lang_code.get_text();
	var range = new Gee.ArrayList<string>();
	range.add("less");
	range.add("less_eq");
	range.add("eq");
	range.add("more_eq");
	range.add("more");
	if (LangCode != "") {
		string output = "";
		try {
			Process.spawn_command_line_sync( "fc-list :lang="  + LangCode , out output, null ,null);
		}
		catch (SpawnError e) {
			stdout.printf("%s", e.message);
		}		
		if (!output.contains(font_name)) {
			stdout.printf("\n%s does not support language: %s", font_name, LangCode);
			LangCode = "";
		}
	}
	string GenFamily = gen_family.get_active_text();
	if (!FontConfList.contains(font_name))
		FontConfList.add(font_name);
	// Save Conf for Font
	Xml.Doc* doc = new Xml.Doc("1.0");
	Xml.Node* root = new Xml.Node(null, "fontconfig");
        doc->set_root_element(root);
	Xml.Node* match1 = root->new_child(null, "match", "");
	match1->set_prop("target", "font");
	Xml.Node* test1 = match1->new_child(null, "test", "");
	test1->set_prop("name", "family");
	test1->new_child(null, "string", font_name);
	// Anialiasing
	Xml.Node* edit1 = match1->new_child(null, "edit", "");
	edit1->set_prop("name", "antialias");
	edit1->set_prop("mode", "assign");
	if (font_smoothing.get_active())
		edit1->new_child(null, "bool", "true");
	else 
		edit1->new_child(null, "bool", "false");
	// Hinting
	Xml.Node* edit2 = match1->new_child(null, "edit", "");
	if (HintMethod == 1) {
		// Set Auto Hinting
		edit2->set_prop("name", "autohint");
		edit2->set_prop("mode", "assign");
		edit2->new_child(null, "bool", "true");
		Xml.Node* edit3 = match1->new_child(null, "edit", "");
		edit3->set_prop("name", "hintstyle");
		edit3->set_prop("mode", "assign");
		edit3->new_child(null, "const", HintStyle);
	}		
	else {
		// Set/Unset Hinting
		edit2->set_prop("name", "hinting");
		if (HintMethod == 0)
			edit2->new_child(null, "bool", "false");
		if (HintMethod == 2)
			edit2->new_child(null, "bool", "true");
	}
	// Synthesis
	if (enable_synth.get_active()) {
		Xml.Node* edit5 = match1->new_child(null, "edit", "");
		edit5->set_prop("name", "matrix");
		edit5->set_prop("mode", "assign");
		Xml.Node* matrix = edit5->new_child(null, "matrix", "");
		matrix->new_child(null, "double", "%.2f".printf(ScaleX));
		matrix->new_child(null, "double", "%.2f".printf(SlantX));
		matrix->new_child(null, "double", "%.2f".printf(SlantY));
		matrix->new_child(null, "double", "%.2f".printf(ScaleY));
	}
	// Aliases
	if (Alias != "") {
		Xml.Node* alias1 = root->new_child(null, "alias", "");
		alias1->new_child(null, "family", Alias);
		Xml.Node* accept = alias1->new_child(null, "accept", "");
		accept->new_child(null, "family", font_name);
	}
	if (GenFamily == "monospace" || GenFamily == "serif" || GenFamily == "sans-serif") {
		Xml.Node* match2 = root->new_child(null, "match", "");
		match2->set_prop("target", "pattern");
		if (LangCode != "") {
			Xml.Node* test2 = match2->new_child(null, "test", "");
			test2->set_prop("name", "lang");
			test2->set_prop("compare", "contains");
			test2->new_child(null, "string", LangCode);
		}
		Xml.Node* test3 = match2->new_child(null, "test", "");
		test3->set_prop("qual", "any");
		test3->set_prop("name", "family");
		test3->new_child(null, "string", GenFamily);
		Xml.Node* edit4 = match2->new_child(null, "edit", "");
		edit4->set_prop("name", "family");
		edit4->set_prop("mode", "prepend");
		edit4->set_prop("binding", "strong");
		edit4->new_child(null, "string", font_name);
	}
	// Advanced Hinting
	if (adv_hint_mode.get_active() && (adv_hint_range_combo.get_active() > 0 || AdvHintRange1 < AdvHintRange2) && (AdvHintMethod != HintMethod) ) {
		Xml.Node* match3 = root->new_child(null, "match", "");
		match3->set_prop("target", "font");
		Xml.Node* test4 = match3->new_child(null, "test", "");
		test4->set_prop("name", "family");
		test4->new_child(null, "string", font_name);
		int idx = adv_hint_range_combo.get_active();
		if (idx == 0 ) {
			Xml.Node* test6 = match3->new_child(null, "test", "");
			test6->set_prop("name", "pixelsize");
			test6->set_prop("compare", "more_eq");
			test6->new_child(null, "int", "%d".printf(AdvHintRange1));
		}
		Xml.Node* test5 = match3->new_child(null, "test", "");
		test5->set_prop("name", "pixelsize");
		if (idx == 0 ) 
			test5->set_prop("compare", "less_eq");
		else 
			test5->set_prop("compare", range[idx-1]);
		test5->new_child(null, "int", "%d".printf(AdvHintRange2));
		Xml.Node* edit6 = match3->new_child(null, "edit", "");
		if (AdvHintMethod == 1) {
			// Set Auto Hinting
			edit6->set_prop("name", "autohint");
			edit6->set_prop("mode", "assign");
			edit6->new_child(null, "bool", "true");
			Xml.Node* edit7 = match3->new_child(null, "edit", "");
			edit7->set_prop("name", "hintstyle");
			edit7->set_prop("mode", "assign");
			edit7->new_child(null, "const", HintStyle);
		}
		else {
			// Set/Unset Hinting
			edit6->set_prop("name", "hinting");
			if (AdvHintMethod == 0)
				edit6->new_child(null, "bool", "false");
			if (AdvHintMethod == 2) {
				edit6->new_child(null, "bool", "true");
				if (HintMethod ==1) {
					Xml.Node* edit8 = match3->new_child(null, "edit", "");
					edit8->set_prop("name", "autohint");
					edit8->set_prop("mode", "assign");
					edit8->new_child(null, "bool", "false");
				}
			}
		}	

	}
	// Advanced  Smoothing
	if (adv_smooth_mode.get_active() && (adv_smooth_range_combo.get_active() > 0 || AdvSmoothRange1 < AdvSmoothRange2 ) && (adv_smooth_enable.get_active() || adv_smooth_disable.get_active()))  {
	Xml.Node* match4 = root->new_child(null, "match", "");
	match4->set_prop("target", "font");
	Xml.Node* test7 = match4->new_child(null, "test", "");
	test7->set_prop("name", "family");
	test7->new_child(null, "string", font_name);
	int idx = adv_smooth_range_combo.get_active();
	if (idx == 0 ) {
		Xml.Node* test8 = match4->new_child(null, "test", "");
		test8->set_prop("name", "pixelsize");
		test8->set_prop("compare", "more_eq");
		test8->new_child(null, "int", "%d".printf(AdvSmoothRange1));
	}
	Xml.Node* test9 = match4->new_child(null, "test", "");
	test9->set_prop("name", "pixelsize");
	if (idx == 0 ) 
		test9->set_prop("compare", "less_eq");
	else 
		test9->set_prop("compare", range[idx-1]);
	test9->new_child(null, "int", "%d".printf(AdvSmoothRange2));
	Xml.Node* edit9 = match4->new_child(null, "edit", "");
	edit9->set_prop("name", "antialias");
	edit9->set_prop("mode", "assign");
		if (font_smoothing.get_active()) { 
			if (adv_smooth_disable.get_active())
				edit9->new_child(null, "bool", "false");
		}
		else {
			if (adv_smooth_enable.get_active())
				edit9->new_child(null, "bool", "true");
		}
	
	}
	// stdout.printf("\nName:%s\nAlias:%s\nScaleX:%1.2f\nScaleY:%1.2f\nSlantX:%1.2f\nSlantY:%1.2f\nHintMethod:%d\nHintStyle:%s\n",  FontName, Alias, ScaleX, ScaleY, SlantX, SlantY, HintMethod, HintStyle);
	string FontConfFile = get_home_dir(FK_CONF_DIR) + "/" + font_name.replace(" ","-") + ".conf";
	string FontConfLink = get_home_dir(FK_CONF_DIR) + "/" + "50-" + font_name.replace(" ","-") + ".conf";
	doc->save_file(FontConfFile);
	if (!FileUtils.test(FontConfLink, FileTest.EXISTS))
		FileUtils.symlink(FontConfFile, FontConfLink);
	SaveFontConfList();
}

public static void  LoadConfList(string filename, string path, Gee.ArrayList<string> conflist) {
	Xml.Doc* doc = Parser.parse_file(filename);
	XPath.Context* xpath = new XPath.Context(doc);
	XPath.Object* result = xpath->eval_expression(path);
	int num =  result->nodesetval->length();
	for (int i = 0; i<num; i++) {
		stdout.printf(result->nodesetval->item(i)->get_content() + "\n");
		conflist.add(result->nodesetval->item(i)->get_content());
	}
}



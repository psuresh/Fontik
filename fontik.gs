[indent=4]

uses Gtk
uses Pango
uses Xml

def  on_mainWindow_destroy () 
    Gtk.main_quit()
		      
def  on_Quit () 
    Gtk.main_quit()

def on_AboutDlg()
    var about = new Gtk.AboutDialog()
    about.set_program_name("Fontik")
    about.set_version("0.6.1")
    about.set_copyright("(c) Suresh P <psuresh@gmx.net>")
    about.set_license("GPLv3\nhttp://www.gnu.org/licenses/gpl.txt")
    about.set_comments("A Tool for Setting Font-wise, User-set Fontconfig Rules")
    about.run()
    about.destroy()

def ShowPane()
    note_book.show()
    frame1.show()

def HidePane()
    note_book.hide()
    frame1.hide()

def SetSampleText(font : string)
    //Display Sample Text
    text : string = SampText
    font_samp_text.set_markup( "<span face=\""+ font + "\">" + text + "</span>")
	
def on_AddFont()
    var dialog = new FontSelectionDialog("Add Fonts")
    var result = dialog.run()
    if result is ResponseType.OK
        var pango_font = FontDescription.from_string(dialog.get_font_name())
        if pango_font is not null
            FontName  = pango_font.get_family()
            if (FontName is "Monospace") or (FontName is "Sans") or (FontName is "Serif" )
                print "\n%s is a Psuedo Family" , (FontName)
            else
                if FontList.contains(FontName)
                    print "\n%s is in the List" , (FontName)	
                else
                    if FontConfList.contains(FontName)
                        print "\n%s is in the Configured List" , (FontName)
                        var msg = new MessageDialog (main_window, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.CLOSE, FontName + " is in the Configured List.\nConfiguration will be overwritten." )
                        msg.run()
                        msg.destroy()
                    // print "font: %s" , FontName
                    FontList.add(FontName)
                    ShowPane()
                    SetSampleText(FontName)
                    font_list.prepend_text(FontName)
                    font_list.set_active(0)
    dialog.destroy()

def on_RemoveFont()
    //Remove The Font from List
    FontName = on_FontList_changed(font_list)
    // print "Font Name " + FontName
    if	FontName is ""
        HidePane()
        combo_entry.set_text("No Font")
        return
    index : int = font_list.get_active()
    font_list.remove_text(index)
    font_list.set_active(0)
    // print "Removing " + FontName
    FontList.remove(FontName)
    if FontList.size is 0
        status_bar.push(0, "")
        HidePane()
        combo_entry.set_text("No Font")
        SetSampleText("Sans")

def on_Update()
    // print "Update clicked"
    var font_name = font_list.get_active_text()
    if font_name is not "" and FontList.contains(font_name)
        SaveConf(font_name)
        combo_entry.set_icon_sensitive(Gtk.EntryIconPosition.SECONDARY, true)
        combo_entry.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, "This Font is Configured")
        status_bar.push(0, "Saved Configuration for " + font_name)
    reset_fc_cache()

def on_btnReset1()
    rb_auto_hint.set_active(true)
    rb_autohint_medium.set_active(true)
    font_smoothing.set_active(true)
    adv_hint_method_combo.set_active(0)
    adv_hint_range_combo.set_active(0)
    adv_smooth_range_combo.set_active(0)
    adv_hint_range1.set_value(6.00)
    adv_hint_range2.set_value(6.00)
    adv_smooth_range1.set_value(6.00)
    adv_smooth_range2.set_value(6.00)
    adv_hint_mode.set_active(false)
    adv_smooth_mode.set_active(false)

def on_btnReset2()
    scale_x.set_value(1.0)
    scale_y.set_value(1.0)
    scale_y.set_adjustment(adj_scale_x)
    uni_scale.set_active(true)
    slant_x.set_value(0.0)
    slant_y.set_value(0.0)
    slant_y.set_adjustment(adj_slant_x)
    uni_slant.set_active(true)
    enable_synth.set_active(false)

def on_btnReset3()
    var alias = get_widget("txtAlias") as Entry
    alias.set_text("")
    gen_family.set_active(0)
    lang_code.set_text("")

def ResetAll()
    on_btnReset1()
    on_btnReset2()
    on_btnReset3()

def on_ResetAllConf()
    // Reset All configured fonts
    var dialog = new Dialog.with_buttons("Reset Fontik Confs", null, Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL, Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK )
    var label = new Label("")
    label.set_markup("Do you want to remove <b>All</b> configurations?\n"  )
    dialog.vbox.add(label)
    dialog.show_all()
    dialog.set_size_request(150,150)
    var result = dialog.run()
    dialog.destroy()
    if result is ResponseType.OK
        // Delete all confs
        print "Removing confs..."
        FontConfList.clear()
        FontBlackList.clear()
        try
            var conf_dir = File.new_for_path (get_home_dir(FK_CONF_DIR))
            var file_list = conf_dir.enumerate_children (FILE_ATTRIBUTE_STANDARD_NAME, 0, null)
            file_info : FileInfo
            while ((file_info = file_list.next_file (null)) != null) 
                var conf_file = file_info.get_name ()
                print "deleting " + conf_file
                if conf_file.has_suffix(".conf")
                    FileUtils.unlink(get_home_dir(FK_CONF_DIR) + "/" + conf_file)
            if FileUtils.test(get_home_dir(FK_FONTS), FileTest.EXISTS)
                FileUtils.unlink(get_home_dir(FK_FONTS))
            if FileUtils.test(get_home_dir(FK_BLACK_LIST), FileTest.EXISTS)
                FileUtils.unlink(get_home_dir(FK_BLACK_LIST))
        except e : GLib.Error
            print "Error: %s\n", e.message
        BlacklistDisabled = false
        reset_fc_cache()

def on_rbAutoHint_toggled()
    if ( rb_auto_hint.get_active() || adv_hint_method_combo.get_active() == 1)
        frm_autohint.set_sensitive(true)
        rb_autohint_medium.set_active(true)
    else
        frm_autohint.set_sensitive(false)

def on_cmbAdvHintMethod_changed()
    if ( rb_auto_hint.get_active() || adv_hint_method_combo.get_active() == 1)
        frm_autohint.set_sensitive(true)
        rb_autohint_medium.set_active(true)
    else
        frm_autohint.set_sensitive(false)    

def on_cbScaleLock_toggled()
    // Enable/Disable Uniform Scaling
    if uni_scale.get_active()
        scale_y.set_value(scale_x.get_value())
        scale_y.set_adjustment(adj_scale_x)
    else
        scale_y.set_adjustment(adj_scale_y)

def on_chkAdvancedHint_toggled()
    if adv_hint_mode.get_active()
        frm_adv_hint.show()
    else
        frm_adv_hint.hide()

def on_chkAdvancedSmooth_toggled()
    if adv_smooth_mode.get_active()
        frm_adv_smooth.show()
    else
        frm_adv_smooth.hide()

def on_chkEnableSmoothing_toggled()
    if font_smoothing.get_active()
        adv_smooth_disable.show()
        adv_smooth_disable.set_active(false)
        adv_smooth_enable.set_active(false)
        adv_smooth_enable.hide()
    else
        adv_smooth_enable.show()
        adv_smooth_enable.set_active(false)
        adv_smooth_disable.set_active(false)
        adv_smooth_disable.hide()

def on_cbSlantLock_toggled()
    // Enable/Disable Uniform Slant
    if uni_slant.get_active()
        slant_y.set_value(slant_x.get_value())
        slant_y.set_adjustment(adj_slant_x)
    else
        slant_y.set_adjustment(adj_slant_y)

def on_cmbAdvHintRange_changed()
    if adv_hint_range_combo.get_active() != 0
        adv_hint_range1.hide()
        lbl_range1.hide()
    else
        adv_hint_range1.show()
        lbl_range1.show()

def on_cmbAdvSmoothRange_changed()
    if adv_smooth_range_combo.get_active() != 0
        adv_smooth_range1.hide()
        lbl_range2.hide()
    else
        adv_smooth_range1.show()
        lbl_range2.show()

def on_chkSynth_toggled()
    if enable_synth.get_active()
        frm_synth.set_sensitive(true)
    else
        frm_synth.set_sensitive(false)

def on_FontList_changed(combo : ComboBoxEntry) : string
    // print "FontList Changed"
    var active_text = combo.get_active_text()
    index : int = combo.get_active()
    var SelectedFont = ""
    if index > -1
        SelectedFont = active_text
        // print "\n%s Selected" , active_text
        SetSampleText(SelectedFont)
        // print "selected " + SelectedFont
        if FontConfList.contains(SelectedFont)
            status_bar.push(0, SelectedFont + " is already Configured")
            combo_entry.set_icon_sensitive(Gtk.EntryIconPosition.SECONDARY, true)
            combo_entry.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, "This Font is Configured")
        else 
            combo_entry.set_icon_sensitive(Gtk.EntryIconPosition.SECONDARY, false)
            combo_entry.set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, "")
            status_bar.push(0, "")
    ResetAll()
    return SelectedFont

